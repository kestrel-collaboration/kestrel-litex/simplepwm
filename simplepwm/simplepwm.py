#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2024 Raptor Engineering, LLC
# License: BSD

import os
import argparse
import subprocess
import tempfile

from migen import *

from litex import get_data_mod

from litex.soc.interconnect import wishbone, stream
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes

kB = 1024
mB = 1024*kB

# SimplePWM Slave interface ------------------------------------------------------------------------------

from litex.soc.interconnect import wishbone, stream
from litex.gen.common import reverse_bytes

class SimplePWMSlave(Module, AutoCSR):
    def __init__(self, platform, pads, endianness="big", pwm_clk_src="pwm", pwm_clk_freq=1e6, pwm_freq=25e3):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)

        # PWM / tach signals
        self.pwm1_out = Signal()
        self.pwm2_out = Signal()
        self.pwm3_out = Signal()
        self.pwm4_out = Signal()
        self.tach1_in = Signal()
        self.tach2_in = Signal()
        self.tach3_in = Signal()
        self.tach4_in = Signal()

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        self.specials += Instance("simple_pwm_wishbone",
            # Parameters
            p_BASE_CLOCK_FREQUENCY_HZ = int(pwm_clk_freq),
            p_REQUIRED_PWM_FREQUENCY_HZ = int(pwm_freq),

            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = slave_bus.adr,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),
            i_pwm_clock = ClockSignal(pwm_clk_src),

            # External signals
            i_tach1_in = self.tach1_in,
            o_pwm1_out = self.pwm1_out,
            i_tach2_in = self.tach2_in,
            o_pwm2_out = self.pwm2_out,
            i_tach3_in = self.tach3_in,
            o_pwm3_out = self.pwm3_out,
            i_tach4_in = self.tach4_in,
            o_pwm4_out = self.pwm4_out,
        )
        # Add Verilog source files
        self.add_sources(platform)

        # External signals
        if hasattr(pads, 'pwm1'):
            self.comb += pads.pwm1.eq(self.pwm1_out)
        self.comb += self.tach1_in.eq(pads.tach1)
        if hasattr(pads, 'pwm2'):
            self.comb += pads.pwm2.eq(self.pwm2_out)
        self.comb += self.tach2_in.eq(pads.tach2)
        if hasattr(pads, 'pwm3'):
            self.comb += pads.pwm3.eq(self.pwm3_out)
        self.comb += self.tach3_in.eq(pads.tach3)
        if hasattr(pads, 'pwm4'):
            self.comb += pads.pwm4.eq(self.pwm4_out)
        self.comb += self.tach4_in.eq(pads.tach4)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "simplepwm").data_location
        platform.add_source(os.path.join(vdir, "simple_pwm_core.v"))
        platform.add_source(os.path.join(vdir, "simple_pwm.v"))

class SimplePWMOutputSlave(Module, AutoCSR):
    def __init__(self, platform, pads, endianness="big", pwm_clk_src="pwm", pwm_clk_freq=1e6, pwm_freq=25e3):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)

        # PWM signals
        self.pwm1_out = Signal()
        self.pwm2_out = Signal()
        self.pwm3_out = Signal()
        self.pwm4_out = Signal()

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        self.specials += Instance("simple_pwm_out_wishbone",
            # Parameters
            p_BASE_CLOCK_FREQUENCY_HZ = int(pwm_clk_freq),
            p_REQUIRED_PWM_FREQUENCY_HZ = int(pwm_freq),

            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = slave_bus.adr,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),
            i_pwm_clock = ClockSignal(pwm_clk_src),

            # External signals
            o_pwm1_out = self.pwm1_out,
            o_pwm2_out = self.pwm2_out,
            o_pwm3_out = self.pwm3_out,
            o_pwm4_out = self.pwm4_out,
        )
        # Add Verilog source files
        self.add_sources(platform)

        # External signals
        self.comb += pads.pwm1.eq(self.pwm1_out)
        self.comb += pads.pwm2.eq(self.pwm2_out)
        self.comb += pads.pwm3.eq(self.pwm3_out)
        self.comb += pads.pwm4.eq(self.pwm4_out)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "simplepwm").data_location
        platform.add_source(os.path.join(vdir, "simple_pwm_core.v"))
        platform.add_source(os.path.join(vdir, "simple_pwm_out.v"))
